<?php
function renderForm()
{
    $form = '<div class="row alignwide">
<div class="col-md-8">
<form action="" id="calculadora_hemcibra" method="POST">
    <div id="ajax-loader"></div>
    <h2 class="main-title">Calculadora Hemcibra</h2>
    <div class="custom_hr"><div class="small_line"></div><div class="big_line"></div></div>
      <span class="span-title">Selecione o estágio da sua dosagem</span>
     <div class="input-group" id="mode">
    <input type="radio" name="mode" id="attack" value="attack"> <label class="styled attack" for="attack" required>Dosagem para Ataque</label>
    <input type="radio" name="mode" id="maintenance" value="maintenance"> <label class="styled maintenance" for="maintenance" required>Dosagem para Manutenção</label> 
    </div>
   
<div class="row">
  <div class="col">
    <div class="collapse multi-collapse" id="dosagecontent">
      <div class="card card-body">
      <div id="mode_message"></div>    
      <span class="small">Preencha o formulário abaixo com seu peso e os frascos que possui:</span>
      <span class="section-instruction">Qual seu peso? (kg)</span>
           <div class="input-group">
    <input type="number" class="default-width" min="5" step="0.5" name="weight" required>
    </div>
    <div class="input-group">
       <select id="flaskSelection" class="">
                <option value="all">Todas apresentações</option>
                <option value="select">Selecionar frascos</option>
            </select>
     </div>

    <div class="input-group maintenance-field">
        <span class="section-instruction">Dose de manutenção</span>'
        . renderMaintenceDosage() .
        '</div>
  
         <div class="available_flasks hide">
    <span class="section-instruction">Selecione o(s) frasco(s) que você possui:</span>
    <div class="input-group">'
        . RenderAvailableVials() .
        '</div>
    </div>
      <div class="divisor"> </div>
        <button class="btn" type="submit" disabled>Complete o formulário para avançar</button>
    </form>
      </div>
    </div>
  </div>

</div>

'.renderResultDiv().'
    </div>'.renderSidebar().'
    </div>
';

    return $form;
}


function renderResultDiv(){
    $div = '<div id="result" class="alignwide"></div>
<div id="result-mode" class="hide"><p class="medium">Refaça o cálculo, selecione o estágio de sua dosagem:</p>
<label class="fake-btn" data-mode="attack">Dosagem para Ataque</label>
<label class="fake-btn" data-mode="maintenance">Dosagem para Manutenção</label>
</div>';
    return $div;
}

function renderMaintenceDosage()
{
    $options = array(1.5, 3, 6);
    $output = ' <div class="input-group">';
    foreach ($options as $option) {
        $output .= '<div class="checkbox-wrapper default-width"><input type="checkbox" name="maintenance_dosage[]" id="id_'.$option.'" value="' . $option . '"><label for="id_'.$option.'">' . $option . '</label></div>';
    }
    $output .= '</div>';
    return $output;
}

function RenderAvailableVials()
{
    $args = array(
        'post_type' => 'flasks',
    );
    $posts = get_posts($args);
    $output = '';
    foreach ($posts as $post) {
        $output .= '<div class="checkbox-wrapper default-width"><input name="available_flasks[]" type="checkbox" id="'.$post->ID.'" value="'.$post->ID.'" required checked="checked"><label for="'.$post->ID.'"><img src="'.get_the_post_thumbnail_url($post).'">' . $post->post_title . '</label></div>';
    }

    return $output;
}