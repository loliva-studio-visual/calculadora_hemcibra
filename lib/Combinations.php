<?php
/**
 * Generate all the possible combinations among a set of nested arrays.
 *
 * @param array $data  The entrypoint array container.
 * @param array $all   The final container (used internally).
 * @param array $group The sub container (used internally).
 * @param mixed $val   The value to append (used internally).
 * @param int   $i     The key index (used internally).
 */
function generate_combinations($array)
{
    // initialize by adding the empty set
    $results = array(array( ));

    foreach ($array as $element)
        if($element != '') {
            foreach ($results as $combination)
                if ($element != '') {
                    array_push($results, array_merge(array($element), $combination));
                }
        }

    return $results;
}