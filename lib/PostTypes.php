<?php
function create_flask_post_type()
{
    register_post_type('flasks',
        array(
            'labels' => array(
                'name' => 'Frascos',
                'singular_name' => 'Frasco',
                'add_new' => 'Adicionar Novo',
                'add_new_item' => 'Adicionar novo frasco',
                'edit' => 'Editar',
                'edit_item' => 'Editar frasco',
                'new_item' => 'Novo frasco',
                'view' => 'Ver',
                'view_item' => 'Ver frasco',
                'search_items' => 'Procurar frascos',
                'not_found' => 'Nenhum frasco encontrado',
                'not_found_in_trash' => 'Nenhum frasco encontrado no lixo',
                'parent' => 'Frasco pai'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => array('title', 'thumbnail'),
            'taxonomies' => array(''),
            'menu_icon' => plugins_url('icon.png', __FILE__),
            'has_archive' => true
        )
    );
}

add_action('init', 'create_flask_post_type');

function custom_meta_box_content($post_id)
{
    $value = get_post_meta($post_id->ID, "mg_hemcibra", true);

    echo '<input type="text" name="mg_hemcibra" value="' . $value . '">';
}

function custom_meta_box_vol($post_id)
{
    $value = get_post_meta($post_id->ID, "vol_hemcibra", true);

    echo '<input type="text" name="vol_hemcibra" value="' . $value . '">';
}

function custom_meta_box_con($post_id)
{
    $value = get_post_meta($post_id->ID, "con_hemcibra", true);

    echo '<input type="text" name="con_hemcibra" value="' . $value . '">';
}


function add_custom_metabox()
{
    add_meta_box(
        "mg_hemcibra",
        "Concentração Mg/Ml",
        "custom_meta_box_content",
        "flasks"
    );

    add_meta_box(
        "vol_hemcibra",
        "Volume do frasco",
        "custom_meta_box_vol",
        "flasks"
    );

    add_meta_box(
        "con_hemcibra",
        "Concentração do frasco (total de mg contido no volume do frasco)",
        "custom_meta_box_con",
        "flasks"
    );
}

add_action("add_meta_boxes", "add_custom_metabox");

function on_save_post($post_id)
{
    $meta_value = isset($_POST["mg_hemcibra"]) ? $_POST["mg_hemcibra"] : false;
    $vol = isset($_POST["vol_hemcibra"]) ? $_POST["vol_hemcibra"] : false;
    $con = isset($_POST["con_hemcibra"]) ? $_POST["con_hemcibra"] : false;


    if (current_user_can("edit_posts") && $meta_value) {
        update_post_meta($post_id, "mg_hemcibra", $meta_value);
    }

    if (current_user_can("edit_posts") && $vol) {
        update_post_meta($post_id, "vol_hemcibra", $vol);
    }

    if (current_user_can("edit_posts") && $con) {
        update_post_meta($post_id, "con_hemcibra", $con);
    }
}

add_action("save_post", "on_save_post");