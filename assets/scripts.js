(function($) {
    $(window).load(function(){

        const $Form = $('#calculadora_hemcibra');
        let contentHtml = $('#dosagecontent').html();
        $Form.validate();

        //allows to submit ajax depending on validation
        $(document).on('change', 'input', function() {


            if ($(this).is(':checkbox')){
               if ($(this).attr('checked')){
                   $(this).parent().removeClass('checked').addClass('checked');
               }
               else {
                   $(this).parent().removeClass('checked');
               }
            }

            if($Form.valid()){
                $Form.find('button').html('Calcular dosagem total').attr('disabled', false);
            }
            else {
                $Form.find('button').html('Complete o formulário para avançar').attr('disabled', true);
            }
        });

        $('#dosagecontent').collapse({
            toggle: false
        });

        $('input[name="mode"]').change(function () {

            $('label.styled').removeClass('clicked');
            $('label.'+$(this).val()).addClass('clicked');
            let $target = $('#dosagecontent');

            //reset all content and validation errors
            $target.empty().prepend(contentHtml);
            $('#result').empty();
            //hide maintenance fields if attack mode
            if ($(this).val() == 'attack'){
                $('.maintenance-field').hide();
                $target.find('#mode_message').html('<span class="title">Dosagem total de ataque (mg) (primeiras 4 semanas)</span>');
            }

            $target.removeClass('show');
            $target.collapse('show');
        });

        $Form.submit(function(e){

            if(!$Form.valid()){
                $Form.find('button').attr('disabled', true);
            }

            e.preventDefault();
            let $payload = $Form.serialize();
            $.ajax({
                url: 'wp-admin/admin-post.php?action=send_data',
                crossDomain: true,
                data: $payload,
                dataType: 'json',
                beforeSend:function(){
                    $('#ajax-loader').show();
                },
                complete: function(data) {
                    applyResultChanges();
                    $('#result').html(data.responseText);

                },
                type: 'POST'
            });
        });


        $('.fake-btn').click(function(){

            $('#mode label.'+$(this).data('mode')).trigger('click');
            $('#result').hide();
            $('#result-mode').hide();
            $('#dosagecontent').collapse('show');
            $('#mode').show();
        });


        $(document).on('change', '#flaskSelection', function(){
           if($(this).val() == 'select'){
               $('.available_flasks').removeClass('hide');
               $('.available_flasks input').each(function(){
                   $(this).prop('checked', false)
               });
           }
           else {
               $('.available_flasks').removeClass('hide').addClass('hide');
               $('.available_flasks input').each(function(){
                   $(this).prop('checked', true)
               });
           }
        });

        function applyResultChanges(){
            $('#result').show();
            $('#ajax-loader').hide();
            $('#dosagecontent').collapse('hide');
            $('#result-mode').show();
            $('#mode').hide();
        }

    });//window load


})(jQuery);

