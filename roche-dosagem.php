<?php
/**
 * Plugin Name: Calculadora Hemcibra
 * Plugin URI: https://www.studiovisual.com.br
 * Description: Calcula doses de acordo com as diretrizes estabelecidas
 * Version: 0.1
 * Text Domain: roche-dosagem
 * Author: Studio Visual
 * Author URI: https://www.studiovisual.com.br
 */

require 'lib/Form.php';
require 'lib/PostTypes.php';
require 'lib/Combinations.php';


add_action('send_headers', 'add_cors_http_header');

function add_cors_http_header()
{
    header("Access-Control-Allow-Origin: *");
}

add_action('wp_enqueue_scripts', 'load_js');
function load_js()
{
    $dir = plugin_dir_url(__FILE__);
    wp_enqueue_script('load_js', $dir . '/assets/scripts.js', array('jquery'));
    wp_enqueue_script('boot2', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), '', true);
    wp_enqueue_script('boot3', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array('jquery'), '', true);
    wp_enqueue_script('validate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js', array('jquery'), '', true);
    wp_enqueue_script('validate-additional', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js', array('jquery'), '', true);
}

add_action('wp_enqueue_scripts', 'load_css');
function load_css()
{
    $dir = plugin_dir_url(__FILE__);
    wp_enqueue_style('load_css', $dir . '/assets/styles.css');
    wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
}

function roche_dosagem_function($atts)
{
    $Content = renderForm();
    return $Content;
}

add_shortcode('roche-dosagem', 'roche_dosagem_function');


add_action('admin_post_send_data', 'send_data');
add_action('admin_post_nopriv_send_data', 'send_data');
function send_data()
{
    calculate($_POST);
}

function calculate($payload)
{
    if ($payload['weight'] == '') {
        $errors = array('error_code' => 1, 'error_message' => 'Peso é obrigatório');
        echo json_encode($errors);
        die;
    }
    if ($payload['mode'] == 'maintenance') {
        echo renderAccordionMaintenance($payload['maintenance_dosage']);
        foreach($payload['maintenance_dosage'] as $dosage) {
            $mgTotal = ($payload['weight'] * $dosage);
            $result = proccessVialWaste($mgTotal, getAvailableFasksMgTotal($payload['available_flasks']));

            renderOutputMaintenance($mgTotal, $result);
        }
    }

    if ($payload['mode'] == 'attack') {
        calculateAttackDosage($payload);
    }

    die;
}

function calculateAttackDosage($payload)
{
    $mgTotal = ($payload['weight'] * 3);
    $result = proccessVialWaste($mgTotal, getAvailableFasksMgTotal($payload['available_flasks']));

    renderOutput($mgTotal, $result);
}

function getAvailableFasksMgTotal($flasksArray)
{
    $availableFlasks = array();
    $flasksMg = array();
    foreach ($flasksArray as $postId) {
        $flask = get_post($postId);
        $availableFlasks[$flask->mg_hemcibra][] = $flask;
        $flasksMg[] = $flask->con_hemcibra;
    }

    return array('total_mg' => array_sum($flasksMg), 'flasks_array' => $availableFlasks);
}

function proccessVialWaste($totalMgNeeded, $availableFlasks)
{
    $selectedVials = array();
    $keys = array();

    foreach ($availableFlasks['flasks_array'] as $con) {
        foreach ($con as $flask) {
            $selectedVials[] = $flask->con_hemcibra;
        }
    }

    $processedWaste = processRest($selectedVials, $totalMgNeeded);

    foreach ($processedWaste as $key => $result) {
        $keys[] = $key;
    }

    //retorna a melhor combinação com menos desperdicio e menos frascos
    return processRest($selectedVials, $totalMgNeeded)[min($keys)];
}

function processRest($selectedVials, $totalMgNeeded)
{
    if (count($selectedVials) == 1) {
        $res = array();
        $ceil = ceil($totalMgNeeded / $selectedVials[0]);
        for ($i = 0; $i < $ceil; $i++) {
            $res[$ceil][] = $selectedVials[0];
        }

        return $res;
    }

    $selectedVials = array_merge($selectedVials, $selectedVials, $selectedVials, $selectedVials);
    $combination = generate_combinations($selectedVials);
    $detailedCombination = array();

    foreach ($combination as $possibility) {
        if (empty($possibility)) {
            continue;
        }
        if (array_sum($possibility) > -1) {
            if ((array_sum($possibility) - $totalMgNeeded) > -1) {
                $rest[] = (array_sum($possibility) - $totalMgNeeded);
                $detailedCombination[(array_sum($possibility) - $totalMgNeeded)][count($possibility)] = $possibility;
            }
        }
    }
    //retorna a melhor combinação com menos desperdício
    return $detailedCombination[min($rest)];
}

function findFlaskByConcentration($con)
{
    $args = array(
        'post_type' => 'flasks',
        'numberposts' => 1,
        'meta_query' => array(
            array(
                'key' => 'con_hemcibra',
                'value' => $con,
                'compare' => 'LIKE'
            )
        )
    );

    return get_posts($args)[0];
}

function getFlaskCount($combination)
{
    $counted = array_count_values($combination);
    $flasks = array();
    foreach ($counted as $key => $flask) {
        $flasks[$key][$flask] = findFlaskByConcentration($key);
    }
    //retorna o array com index indicando qts frascos e com o obj como value
    return $flasks;
}


function processFlasksMl($result, $mgTotal)
{

    $remainingMg = $mgTotal;
    $outPut = array();
    arsort($result);

    foreach ($result as $flask) {

        $flaskObj = findFlaskByConcentration($flask);
        $fixedVolume = floatval(str_replace(',', '.', $flaskObj->vol_hemcibra));

        //find the ml needed
        $totalRemainingInMl = $remainingMg / $flaskObj->mg_hemcibra;
        $totalMl = ($totalRemainingInMl >= $fixedVolume) ? $fixedVolume : $totalRemainingInMl;

        $remainingMg = $remainingMg - $flaskObj->con_hemcibra;

        if (!flaskAlreadyExistsInArray($outPut['not_repeated'], $totalMl, $flaskObj->con_hemcibra)) {
            $outPut['not_repeated'][] = ['ml' => $totalMl, 'flask' => $flaskObj->con_hemcibra];
        }

        $outPut['full'][] = ['ml' => $totalMl, 'flask' => $flaskObj->con_hemcibra];
    }

    return $outPut;
}

function flaskAlreadyExistsInArray($array, $flask_con, $flask_vol){
    foreach($array as $flask) {
        $res = (array_search($flask_con, $flask, false) && array_search($flask_vol, $flask, false));
           if ($res){
               return true;
           }
    }
}

function formatDosageHtml($data)
{
    foreach ($data['not_repeated'] as $flask) {
        $defaultFlask = getDefaultFlaskConfiguration($flask['flask']);

        if ($defaultFlask['flask_vol'] != $flask['ml']){
            $message = 'Utilize ' . $flask['ml'] . 'ml do último frasco de ' . $flask['flask'] . '.';
        }
        else {
            $message = 'Utilize ' . $flask['ml'] . 'ml de cada frasco de ' . $flask['flask'] . '.<br>';
        }
        echo $message;
    }

}

function getDefaultFlaskConfiguration($flaskConcentration)
{
    $flask = findFlaskByConcentration($flaskConcentration);
    return array('flask_vol' => str_replace(',', '.', $flask->vol_hemcibra), 'flask_con' => $flask->con_hemcibra);

}

function formatHtml($qty, $obj)
{
    $html = '';
    for ($i = 0; $i < $qty; $i++) {
        $html .= '<img src="' . get_the_post_thumbnail_url($obj) . '">';
    }
//    $html .= '<div class="flask-wrapper"><div class="qtd">' . $qty . 'x</div><div class="flask-obj">' . $obj->con_hemcibra . '</div></div>';
    return $html;
}

function renderOutputMaintenance($mgtotal, $result)
{

    echo '  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample1">
      <div class="card card-body">
        '.renderOutput($mgtotal, $result).
        '
      </div>
    </div>
  </div>';
}

function renderOutput($mgtotal, $result)
{
    $advice = '<b>Recomendação: <br></b>';
    $lastElement = end(getFlaskCount($result));
    $flasksVol = null;

    foreach (getFlaskCount($result) as $con => $arrayflask) {
        foreach ($arrayflask as $qty => $obj) {
            $flasksVol .= str_replace(',','.',$obj->vol_hemcibra)*$qty.'|';

            echo formatHtml($qty, $obj);
            $advice .= 'use '.$qty.' frasco(s) de '.$obj->post_title.'<br>';
        }
        if ($arrayflask != $lastElement){
            $advice .= 'e ';
        }
    }

    $flasksVol = floatval(array_sum(explode('|',$flasksVol)));
    $advice .= 'para obter o total disponível de '.$flasksVol.'ml e '.sumFlasksMgTotal($result).'mg.<br><br><b>Recomendação de distribuição nas seringas</b><br>';


    $output =  $advice;
    $totalMlNeeded = array_sum(array_column(processFlasksMl($result, $mgtotal)['full'], 'ml'));

    echo '<div id="result-wrapper"><div class="triangle"></div>';
    echo $output;
    echo formatDosageHtml(processFlasksMl($result, $mgtotal)).'<div clas="mgTotal">Para totalizar '. $totalMlNeeded.'ml e ' . $mgtotal . 'mg.</div>';
    echo '</div><p class="small">* Diferentes concentrações dos frascos-ampola de HEMCIBRAmd (emicizumabe) (30mg/mL e 150mg/mL) não devem ser combinadas na mesma seringa.</p>';
}

function sumFlasksMgTOtal($result){
    return (array_sum($result));
}

function renderAccordionMaintenance($opts){

    $output = '<p>';
    foreach($opts as $opt){
        $output .= '<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#tgt_'.$opt.'" aria-expanded="false" aria-controls="tgt_'.$opt.'">Dose '.$opt.'</button>';
    }
    $output .='</p>';

    return $output;

}

function renderSidebar()
{
    return '<div class="col-md-4">teste</div>';
}